public class Overloading1 {
    private static int i = 1;
    private static double l = 3.1415;
    private static String k = "我是栗子";
    public void test(int a){
        System.out.println("你输入了整数:"+a);
    }   
    public void test(double a){
        System.out.println("你输入了浮点数:"+a);
    }   
    public void test(String a){
        System.out.println("你输入了字符串:"+a);
    }   
    public static void main(String[] args){
        Overloading1 lizi = new Overloading1();
        lizi.test(i);
        lizi.test(l);
        lizi.test(k);
    }
}
