class Hello{
	public static void main(String[] args){
		System.out.println("hello,hhhjava");
		Card card4 = new Card("四");
		Card card5 = new Card("四");
		System.out.println(card4.name);
		card4.name = "六";
		System.out.println(card4.name);
		System.out.println(card5.name);
	}
}

// 什么是类：
// 就是同一类事物的模板，描述了事物的状态和行为，是抽象概念。
// 什么是对象：
// 对象是真实存在的，是类的实例。具有自身的状态和行为。
// 二者联系：类是对象的抽象，对象是类的实例。
// 状态：成员变量/属性
// 行为：成员函数/函数
//
// 如何定一个类
// class 类名{}
//
class Card{
	String name;
	Card(String name){
		this.name = name;
	}
}
