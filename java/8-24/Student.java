class Person{
	protected String name;
	public String getName(){
		return name;
	}
}


class abstract Driver{
	abstractvoid drive();
}

class Student extends Person{
	private int percent;
	
	public Student(String name,int percent){
		this.name = name;
		this.percent = percent;
	}


	public String getLevel(){
		String level = "A";
		if(percent < 60){
			level = "C";
		}else if(percent < 80){
			level = "B";
		}
		return level;
	}	
}

